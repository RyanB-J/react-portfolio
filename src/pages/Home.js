import React from 'react'
import { Link } from 'react-scroll'
import WOW from "wow.js"
import Projects from '../components/Projects'
import Footer from '../components/Footer'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import '../assets/styles/animate.css'
import '../assets/styles/home.css'

class Home extends React.Component {
    componentDidMount() {
        let wow = new WOW()
        wow.init()
    }

    imageCredit = <span>
            Background image made by
            <a href="https://www.pexels.com/photo/silhouette-photography-of-rocky-mountains-under-starry-sky-2565420/"
               target="_blank" rel="noopener noreferrer">
                &nbsp;Fabio Marciano
            </a>.
        </span>

    render() {
        return (
            <main id={'home'}>
                <section id={'header'}>
                    <div className={'container text-center text-white'}>
                        <div className={'jumbotron'}>
                            <h1 className={'display-2 animated wow fadeInDown'}>
                                React Portfolio
                            </h1>
                            <p className={'lead animated wow fadeInUp'}>By Ryan Bains-Jordan</p>
                            <Link className={'btn btn-lg btn-outline animated wow fadeInUp'}
                                  to={'projects'}
                                  spy={true}
                                  smooth={true}
                                  duration={500}
                                  data-wow-delay={'0.8s'}>
                                My Projects
                            </Link>
                        </div>
                    </div>
                </section>
                <section id={'about'}>
                    <div className={'container text-white'}>
                        <div className={'row'}>
                            <div id={'about-left'} className={'col-sm-6 animated wow fadeInLeft'} data-wow-delay={'0.5s'}>
                                <h1 id={'about-logo'}><FontAwesomeIcon icon={['fab', 'react']}/></h1>
                            </div>
                            <div id={'about-right'} className={'col-sm-6 animated wow fadeInRight'} data-wow-delay={'0.5s'}>
                                <p className={'lead'}>
                                    A collection of small applications created with the React.js framework. All of them
                                    use the React Fetch API to accomplish an assortment of tasks.
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
                <section style={{background: 'linear-gradient(to right bottom, black 49%, #35007A 50%)', height: '4rem'}}>
                </section>
                <section id={'projects'}>
                    <div className={'container text-center'}>
                        <h1 className={'display-4 w-100 animated wow fadeInUp'} data-wow-delay={'0.2s'}>Apps</h1>
                        <p className={'lead w-100 animated wow fadeInUp'} data-wow-delay={'0.5s'}>
                            A list of some miniature applications that I created using Todd Motto's
                            Public API list Github repository. Link found&nbsp;
                            <a href="https://github.com/public-apis/public-apis" target="_blank"
                               rel="noopener noreferrer">here</a>.
                        </p>
                        {/*APIs: https://github.com/public-apis/public-apis*/}
                        <div id={'projects-list'}>
                            <Projects />
                        </div>
                    </div>
                </section>
                <Footer slot={this.imageCredit} />
            </main>
        )
    }
}

export default Home