import React from 'react'

const Trailer = ({ animeShow }) => {
    if (!animeShow) return null
    else if (animeShow.trailer === null
        || animeShow.trailer.site !== 'youtube') return null
    const id = animeShow.trailer.id

    return(
        <section id={'trailer'}>
            <div className={'container'}>
                <iframe
                    src={'https://www.youtube.com/embed/' + id}
                    frameBorder="0"
                    allow={'autoplay; encrypted-media'}
                    title={'Trailer for ' + animeShow.title.english}
                    allowFullScreen
                />
            </div>
        </section>
    )
}

export default Trailer