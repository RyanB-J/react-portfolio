import React from 'react'

class Search extends React.Component {
    state = { animeQuery: ''}

    updateAnimeQuery = event => {
        this.setState({ animeQuery: event.target.value })
    }

    handleKeyPress = event => {
        if (event.key === 'Enter') {
            this.searchAnime(event)
        }
    }

    searchAnime = event => {
        event.preventDefault()
        this.props.searchAnime(this.state.animeQuery)
    }

    render() {
        return (
            <section id={'search'}>
                <div className="container">
                    <form>
                        <div className={'input-group'}>
                            <input type={'text'}
                                   className={'form-control'}
                                   inputMode={'search'}
                                   placeholder={'Search for an Anime'}
                                   onKeyPress={this.handleKeyPress}
                                   onChange={this.updateAnimeQuery}
                            />
                            <div className={'input-group-append'}>
                                <button
                                    className={'btn btn-outline-light'}
                                    onClick={this.searchAnime}
                                >Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        )
    }
}

export default Search