import React from 'react'
import slugify from '../../assets/scripts/slugify'

const Show = ({ animeShow }) => {
    if (!animeShow) return null
    const {title, coverImage, description, genres, episodes, externalLinks} = animeShow

    return (
        <div id={'show'}>
            <div className="row">
                <div className="col-sm-3">
                    <img src={coverImage.large} alt={'anime cover art'}/>
                    <p className={'mb-0'}>
                    {
                        genres.map(genre => {
                            return (
                                <span key={genres.indexOf(genre)}>{genre} </span>
                            )
                        })
                    }
                    </p>
                    <p className="lead">{episodes} episodes</p>
                </div>
                <div className="col-sm-9">
                    <h1 className={'display-4'}>{title.romaji}</h1>
                    <h1>{title.english}</h1>
                    <p dangerouslySetInnerHTML={{ __html: description }}/>
                    {
                        externalLinks.map(link => {
                            const {id, site, url} = link
                            return(
                                <a
                                    key={id}
                                    href={url}
                                    className={'btn ' + slugify(site)}
                                    target={'_blank'}
                                    rel="noopener noreferrer"
                                >{site}</a>
                            )
                        })
                    }
                </div>
            </div>
        </div>
    )
}

export default Show