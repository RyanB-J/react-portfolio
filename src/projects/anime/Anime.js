import React from 'react'
import axios from 'axios'

import PROJECTS from '../../assets/data/projects'
import Search from './Search'
import Show from './Show'
import Characters from "./Characters";
import Trailer from "./Trailer"

import Footer from "../../components/Footer";

import '../../assets/styles/anime.css'

class Anime extends React.Component {
    state = { queryData: null }

    // Randomly set the results to a specific query
    componentDidMount() {
        let animeList = PROJECTS[1].defaults
        this.searchAnime(animeList[Math.floor(Math.random()*animeList.length)])
    }

    // Set the query for a request to the AniList API
    searchAnime = animeQuery => {
        // The GraphQL query
        const query = `
        query($queryString: String!) {
            Page {
                media(search: $queryString) {
                    title {
                        romaji
                        english
                    }
                    coverImage {
                        large
                    }
                    characters(page:1) {
                        edges {
                            node {
                                id
                                image { large }
                                name { first, last }
                            }
                            voiceActors(language: JAPANESE) {
                                name { first, last }
                            }
                        }
                    }
                    genres
                    description
                    episodes
                    trailer {
                        id
                        site
                    }
                    externalLinks {
                        id
                        site
                        url
                    }
                }
            }
        }`
        // Set the animeQuery to the queryString variable
        const variables = {queryString: animeQuery}

        // Call the method to execute the async function
        this.getAnime(query, variables)
    }

    // Get the anime data from the API
    getAnime = async (query, variables) => {
        try {
            const response = await axios.post(`https://graphql.anilist.co`, {
                query,
                variables
            })

            //console.log(response.data)

            // Set the data to the state
            this.setState(() => ({
                queryData: response.data.data.Page.media[0]
            }))

            // Log the response
            console.log(this.state.queryData)

        } catch (error) {
            console.log('Query Error: ', error)
        }
    }

    render() {
        return (
            <main id={'anime'}>
                <section id={'header'}>
                    <div className={'jumbotron'}>
                        <div className={'container'}>
                            {/*https://www.pexels.com/photo/brown-haired-female-anime-character-figure-164839/*/}
                            <h1 className={'display-4'}>{PROJECTS[1].title}</h1>
                            <p className={'lead'}>{PROJECTS[1].description}</p>
                        </div>
                    </div>
                </section>
                <Search searchAnime={this.searchAnime}/>
                <section id={'content'} className={'container'}>
                    <Show animeShow={this.state.queryData}/>
                    <Characters animeShow={this.state.queryData}/>
                </section>
                <Trailer animeShow={this.state.queryData}/>
                <Footer/>
            </main>
        )
    }
}

export default Anime