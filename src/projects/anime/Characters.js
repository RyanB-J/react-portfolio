import React from 'react'

const Characters = ({ animeShow }) => {
    if (!animeShow) return null
    const { characters } = animeShow

    return (
        <React.Fragment>
           <h1>Characters</h1>
            <div id={'characters'} >
                {
                    characters.edges.map(character => {
                        const {node, voiceActors} = character

                        // Concatenate names
                        let characterName = node.name.first.concat(" ", node.name.last)
                        var voiceName = null
                        if ( voiceActors[0] != null ) {
                            voiceName = voiceActors[0].name.first.concat(" ", voiceActors[0].name.last)
                        }

                        return (
                            <div className={'card character-card'} key={node.id}>
                                <img src={node.image.large} alt="character profile"/>
                                <div className="card-body">
                                    <h5 className="card-title" title={characterName}>
                                        {characterName.substring(0, 14) + (characterName.length > 14 ? '...' : '')}
                                    </h5>
                                    <h6 className={'text-muted'} title={voiceName}>
                                        {
                                            (voiceName ?
                                                voiceName.substring(0, 14) + (voiceName.length > 14 ? '...' : '') :
                                                'UNAVAILABLE')
                                        }
                                    </h6>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </React.Fragment>
    )
}

export default Characters