import React from 'react'

import PROJECTS from '../../assets/data/projects'
import Search from './Search'
import Artist from './Artist'
import Tracks from './Tracks'
import Footer from '../../components/Footer'

import '../../assets/styles/music.css'

const API_ADDRESS = 'https://spotify-api-wrapper.appspot.com'

class Music extends React.Component {
    state = { artist: null, tracks: null }

    // Randomly set the results to a specific query
    componentDidMount() {
        let musicList = PROJECTS[0].defaults
        this.searchArtist(musicList[Math.floor(Math.random()*musicList.length)])
    }

    searchArtist = artistQuery => {

        /* Get the artist from the search query
         * https://spotify-api-wrapper.appspot.com/artist/bruno
         * API:
         * "artists": {
         *   ...
         *   "items": [],
         *   ...
         *   "total": 1
         * }
         */
        fetch(`${API_ADDRESS}/artist/${artistQuery}`)
            .then(response => response.json())
            .then(json => {

                // If there is a result
                if (json.artists.total > 0) {
                    let artist = json.artists.items[0]
                    this.setState({ artist })

                    // Get the tracks from the associated artist
                    fetch(`${API_ADDRESS}/artist/${artist.id}/top-tracks`)
                        .then(response => response.json())
                        .then(json => this.setState({ tracks: json.tracks }))
                        .catch(error => alert(error.message))
                }
            })
    }

    render() {
        console.log('this.state', this.state)

        return(
            <main id={'music'}>
                <section id={'header'}>
                    <div className={'jumbotron'}>
                        <div className={'container'}>
                            {/*https://www.pexels.com/photo/audio-e-guitars-guitars-music-6966/*/}
                            <h1 className={'display-4'}>{PROJECTS[0].title}</h1>
                            <p className={'lead'}>{PROJECTS[0].description}</p>
                        </div>
                    </div>
                </section>
                <Search searchArtist={this.searchArtist}/>
                <section id={'content'}>
                    <Artist artist={this.state.artist} />
                    <Tracks tracks={this.state.tracks} />
                 </section>
                <Footer />
            </main>
        )
    }
}

export default Music