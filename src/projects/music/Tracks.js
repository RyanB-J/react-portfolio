import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Tracks extends React.Component {
    state = { playing: false, audio: null, playingPreviewUrl: null }

    playAudio = previewUrl => () => {
        const audio = new Audio(previewUrl)
        audio.volume = 0.2

        if (!this.state.playing) {
            audio.play()
            this.setState({playing: true, audio, playingPreviewUrl: previewUrl})
        } else {
            this.state.audio.pause()

            if (this.state.playingPreviewUrl === previewUrl) {
                this.setState({ playing: false })
            } else {
                audio.play()
                this.setState({audio, playingPreviewUrl: previewUrl})
            }
        }
    }

    trackIcon = track => {
        if (!track.preview_url) {
            return <FontAwesomeIcon icon={['fas', 'ban']}/>
        }

        if (this.state.playing && this.state.playingPreviewUrl === track.preview_url) {
            return <FontAwesomeIcon icon={['fas', 'pause']}/>
        }
        return <FontAwesomeIcon icon={['fas', 'play']}/>
    }

    render() {
        const { tracks } = this.props
        if (!tracks) return null

        return(
            <div className={'container'}>
                <h1>Top Tracks</h1>
                <div id={'tracks'}>
                    {
                        tracks.map(track => {
                            const { id, name, album, preview_url } = track
                            return (
                                <div className={'track'} key={id} onClick={this.playAudio(preview_url)}>
                                    <img src={album.images[0].url} alt={'album-art'} />
                                    <p>{name.substring(0, 20) + (name.length > 20 ? '...' : '')}</p>
                                    <span className={'track-icon'}>{this.trackIcon(track)}</span>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

export default Tracks