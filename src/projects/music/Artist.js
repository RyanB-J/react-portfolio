import React from 'react'

const Artist = ({ artist }) => {
    if (!artist) return null
    const {images, name, followers, genres, external_urls} = artist

    return (
        <div id={'artist'} className={'container'}>
            <img src={images[0] && images[0].url} alt="artist-profile"/>
            <h1 className={'display-4'}>{name}</h1>
            <p className={'lead'}>{followers.total} followers</p>
            <p>{genres.join(',')}</p>
            <a href={external_urls.spotify}
               className={'btn'}
               target={'_blank'}
               rel="noopener noreferrer">
                View on Spotify
            </a>
        </div>
    )
}

export default Artist