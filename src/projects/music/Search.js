import React from 'react'

class Search extends React.Component {
    state = { artistQuery: '' }

    updateArtistQuery = event => {
        this.setState({ artistQuery: event.target.value })
    }

    handleKeyPress = event => {
        if (event.key === 'Enter') {
            this.searchArtist(event)
        }
    }

    searchArtist = event => {
        event.preventDefault()
        this.props.searchArtist(this.state.artistQuery)
    }

    render() {
        return (
            <section id={'search'}>
                <div className="container">
                    <form>
                        <div className={'input-group'}>
                            <input type={'text'}
                                   className={'form-control'}
                                   inputMode={'search'}
                                   placeholder={'Search for an Artist'}
                                   onKeyPress={this.handleKeyPress}
                                   onChange={this.updateArtistQuery}
                            />
                            <div className={'input-group-append'}>
                                <button
                                    className={'btn btn-outline-light'}
                                    onClick={this.searchArtist}
                                >Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        )
    }
}

export default Search