import React from 'react'
import axios from 'axios'

import PROJECTS from '../../assets/data/projects'
import Search from './Search'
import Profile from './Profile'
import Footer from '../../components/Footer'

import '../../assets/styles/stocks.css'

class Stocks extends React.Component {
    state = { profileData: null, graphData: null, currentDate: null, previousDate: null }

    componentDidMount() {
        let date = new Date()
        this.setState({currentDate:  date.getFullYear() + '-' + date.getMonth() + '-' + date.getDay()})
        this.setState({previousDate: (date.getFullYear() - 1) + '-' + date.getMonth() + '-' + date.getDay()})
        this.getStock(PROJECTS[2].defaults)
    }

    getStock = stockQuery => {
        axios.get(`https://financialmodelingprep.com/api/v3/company/profile/${stockQuery}`)
            .then(response => {
                let data = response.data.profile
                this.setState({ profileData: data })
                console.log('profileData', this.state.profileData)
            })
    }

    getHistory = stockQuery => {
        axios.get(`https://financialmodelingprep.com/api/v3/historical-price-full/${stockQuery.toUpperCase()}
            ?serietype=line&from=${this.state.previousDate}&to=${this.state.currentDate}`)
            .then(response => {
                let data = response.data.historical
                this.setState({ graphData: data })
                console.log('graphData', this.state.graphData)
            })
    }

    render() {
        return(
            <main id={'stocks'}>
                <section id={'header'}>
                    <div className={'jumbotron'}>
                        <div className={'container'}>
                            <h1 className={'display-4'}>{ PROJECTS[2].title }</h1>
                            <p className={'lead'}>{ PROJECTS[2].description }</p>
                        </div>
                    </div>
                </section>
                <Search getStock={this.getStock} getHistory={this.getHistory}/>
                <Profile profile={this.state.profileData} graphData={this.state.graphData}/>
                <Footer/>
            </main>
        )
    }
}

export default Stocks