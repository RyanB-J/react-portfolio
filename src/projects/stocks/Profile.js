import React from 'react'
import Graph from './Graph'

const Profile = ({ profile, graphData }) => {
    if (!profile) return null
    const {companyName, description, image, exchange, industry, sector, price, changes, changesPercentage, beta, volAvg,
        mktCap, latestDiv, ceo, website} = profile

    return (
        <section id={'profile'}>
            <div className={'container'}>
                <div id={'top-area'} className={'row'}>
                    <div id={'symbol-icon'} className="col-sm-2">
                        <img src={image} alt="symbol logo"/>
                    </div>
                    <div id={'symbol-name'} className={'col-sm-7'}>
                        <h3>{companyName}</h3>
                    </div>
                    <div id={'symbol-value'} className={'col-sm-3'}>
                        <span style={(changes < 0 ? {color: 'red'} : {color: 'green'})}>
                            {(changes < 0 ? '' : '+').concat(changes)}
                        </span>&nbsp;
                        <span style={
                            (changesPercentage.match(/^\(-/g) != null
                                ? {color: 'red'} : {color: 'green'})}>
                            {changesPercentage}
                        </span>
                    </div>
                </div>
                {/*<Graph data={graphData}/>*/}
                <hr />
                <p>{description}</p>
                <div className={'row'}>
                    <div className={'col-md-6'}>
                        <table className={'table'}>
                            <tbody>
                            <tr>
                                <th>Industry: </th>
                                <td>{industry}</td>
                            </tr>
                            <tr>
                                <th>Sector: </th>
                                <td>{sector}</td>
                            </tr>
                            <tr>
                                <th>Exchange: </th>
                                <td>{exchange}</td>
                            </tr>
                            <tr>
                                <th>CEO: </th>
                                <td>{ceo}</td>
                            </tr>
                            <tr>
                                <th>Website: </th>
                                <td>
                                    <a href={website}
                                       target={'_blank'}
                                       rel="noopener noreferrer">
                                        {website}
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className={'col-md-6'}>
                        <table className={'table'}>
                            <tbody>
                            <tr>
                                <th>Price: </th>
                                <td>{price}</td>
                            </tr>
                            <tr>
                                <th>Market Cap: </th>
                                <td>{mktCap}</td>
                            </tr>
                            <tr>
                                <th>Latest Dividend: </th>
                                <td>{latestDiv}</td>
                            </tr>
                            <tr>
                                <th>Beta: </th>
                                <td>{beta}</td>
                            </tr>
                            <tr>
                                <th>Volumetric Average: </th>
                                <td>{volAvg}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Profile