import React from 'react'

class Search extends React.Component {
    state = { stockQuery: ''}

    updateStockQuery = event => {
        this.setState({ stockQuery: event.target.value })
    }

    handleKeyPress = event => {
        if (event.key === 'Enter') {
            this.searchStocks(event)
        } else {
            return (event.key >= 'a' && event.key <= 'z')
                || (event.key >= 'A' && event.key <= 'Z')
        }
    }

    searchStocks = event => {
        event.preventDefault()
        this.props.getStock(this.state.stockQuery)
        this.props.getHistory(this.state.stockQuery)
    }

    render() {
        return (
            <section id={'search'}>
                <div className="container">
                    <form>
                        <div className={'input-group'}>
                            <input type={'text'}
                                   className={'form-control'}
                                   placeholder={'Enter symbol'}
                                   onKeyPress={this.handleKeyPress}
                                   onChange={this.updateStockQuery}
                            />
                            <div className={'input-group-append'}>
                                <button
                                    className={'btn btn-outline-light'}
                                    onClick={this.searchStocks}
                                >Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        )
    }
}

export default Search