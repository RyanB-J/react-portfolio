import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.css'
import './assets/styles/main.css'

/***** Pages/Projects *****/
import Home from './pages/Home'
import Music from './projects/music/Music'
import Anime from './projects/anime/Anime'
import Stocks from './projects/stocks/Stocks'

/***** Font Awesome Library *****/
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
library.add( fas, far, fab )

/***** Main Component *****/
class App extends React.Component {
  render() {
    return(
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/music' component={Music}/>
          <Route path='/anime' component={Anime}/>
          <Route path='/stocks' component={Stocks}/>
        </Switch>
      </BrowserRouter>
    )
  }
}

export default App
