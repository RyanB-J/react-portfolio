import React from 'react'
import PROJECTS from '../assets/data/projects'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Projects extends React.Component {
    render() {
        return (
            PROJECTS.map(PROJECT => {
                return (
                    <Link key={PROJECT.id} to={PROJECT.href}>
                        <article className={'card'}>
                            <div className={'card-icon'}>
                                <FontAwesomeIcon icon={PROJECT.icon}/>
                            </div>
                            <div className={'card-body'}>
                                <h5 className={'card-title'}>{PROJECT.title}</h5>
                                <p className={'card-text'}>
                                    {PROJECT.description}
                                </p>
                            </div>
                        </article>
                    </Link>
                )
            })
        )
    }
}

export default Projects