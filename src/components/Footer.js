import React from 'react'

const Footer = props => {
    return(
        <footer>
            <p>
                &copy; Copyright 2019 Ryan Bains-Jordan. <br/>
                Powered by React.js, React Router, Node.js, Axios, GraphQL, Bootstrap 4 and Font Awesome.<br />
                { props.slot }
            </p>
        </footer>
    )
}

export default Footer