// Icons using React-Fontawesome Library
// https://www.npmjs.com/package/@fortawesome/react-fontawesome

const PROJECTS = [
    {
        id: 1,
        title: 'Music Master',
        icon: ['fas', 'music'],
        description: 'Music search engine from the Spotify API',
        defaults: ['pentatonix', 'enya', 'dido', 'jonny cash', 'alanis morissette', 'green day', 'awolnation',
            'talking heads', 'cyndi lauper', 'celine dion', 'coldplay', 'fun.', 'linkin park', 'sia', 'michelle branch',
            'muse', 'nickelback', 'red hot chili peppers', 'sixpence none the richer'],
        href: '/music'
    },
    {
        id: 2,
        title: 'AniWow',
        icon: ['fas', 'tv'],
        description: 'Anime Search Engine for all things Anime',
        defaults: ['cowboy bebop', 'fullmetal alchamist: brotherhood', 'one punch man', 'sword art online', 'bleach',
            'nanatsu no taizai', 'death note', 'attack on titan', 'spirited away', 'ouran high school host club'],
        href: '/anime'
    },
    {
        id: 3,
        title: 'Stocker',
        icon: ['fas', 'chart-line'],
        description: 'The simple Stock Market Symbol Tracker for exchange research.\n' +
            'Powered by Financial Modeling Prep.',
        defaults: 'hpe',
        href: '/stocks'
    }
]

export default PROJECTS